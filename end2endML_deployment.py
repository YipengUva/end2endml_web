# %% load required packages
# web application related packages
import streamlit as st
import tkinter as tk
from streamlit_pandas_profiling import st_profile_report

# import end2endML related package
import os
import joblib
import pandas as pd
import copy
from end2endML.preprocessing import data_preprocessing
from end2endML.utilities import intersection, setdiff, split_data
from end2endML.automate_modeling_evaluation import automate_modeling
from end2endML.automate_modeling_evaluation import automate_evaluation
from end2endML.evaluate_models.classification_metrics import (
    classification_evaluation_test_,
)
from end2endML.evaluate_models.regression_metrics import regression_evaluation_test_

# import utility functions to support the wbe application
import codes.utilities as utilities
from codes.extract_features_from_models import extract_features
from codes import modeling_evaluating
from codes.generate_ROCs import binary_ROCs_evaluation

import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import seaborn as sns

# %% Applying the saved models for future useage
st.title("Deploy trained ML models for future data prediction")

# %% introduction page
st.header("Introduction")

# read into the introduction file at the begining of the web application
current_path = os.path.dirname(os.path.abspath(__file__))
if st.checkbox("Show the introduction content"):
    st.markdown(
        utilities.get_file_content_as_string(
            os.path.join(current_path, "docs/introduction_deployment.md")
        )
    )

# %% Load saved preprocessing steps and trained models
st.header("Load saved preprocessing steps and selected models")

## Working directory
st.markdown("**Specify the working directory to load the saved results**")

## folder picker button
import tkinter as tk

# Set up tkinter
root = tk.Tk()
root.withdraw()

# Make folder picker dialog appear on top of other windows
root.wm_attributes("-topmost", 1)

# Folder picker button
clicked = st.button("Folder Picker")

# when root_path is not specified, stop the web application without continuing
if clicked:
    dirname = st.text_input(
        "Selected folder:", value=tk.filedialog.askdirectory(master=root)
    )

    # using state variable to keep the state of root_path
    if "root_path" not in st.session_state:
        st.session_state["root_path"] = dirname
    # when user specified a different folder in a new iteration, change the root_path
    if dirname != st.session_state["root_path"]:
        st.session_state["root_path"] = dirname

if "root_path" not in st.session_state:
    st.warning(
        "Please specify the working directory to load your saved data preprocessing steps and selected models"
    )
    st.stop()

# load the saved preprocessing steps and selected models
target_path = st.session_state["root_path"]
path2results = os.path.join(target_path, "saved_selected_models.joblib")
path2preprocess = os.path.join(target_path, "saved_preprocess_steps.joblib")
saved_results = joblib.load(path2results)
saved_preprocess_steps = joblib.load(filename=path2preprocess)
st.success(
    "The path to the saved preprocessing steps and selected models is specificied."
)

# %% Upload your data set
st.header("Upload your raw data")

# Upload the raw data for prediction
st.markdown("**Please upload your data:**")
uploaded_future_file = st.file_uploader("Upload your new data set", type=["csv", "zip"])
if uploaded_future_file:
    # infer the compression style
    compression = utilities.infer_compression(uploaded_future_file.name)
    new_data = utilities.load_data(uploaded_future_file, compression)
    st.write(f"The size of the uploaded new data is `{new_data.shape}`")

if not uploaded_future_file:
    st.warning("Please upload your data for prediction")
    st.stop()
st.success("You data is ready for making prediction.")

# %% Preprocess the raw data
st.header("Making prediction on the uploaded data")

# Applied saved preprocessing steps to preprocess the new raw data
st.subheader("Preprocess the raw data")
X_future = saved_preprocess_steps.transform(new_data)
st.markdown(f"The size of the preprocessed data size is {X_future.shape}")

# Select the model you want to apply
inverse_model_names = dict((value, key) for key, value in utilities.model_names.items())
model_names = [ele.split("_")[-1] for ele in saved_results.keys()]
model_names = [inverse_model_names[ele] for ele in model_names]
model_names_dict = {
    key: value for key, value in zip(model_names, list(saved_results.keys()))
}

st.markdown("**Select the models you want to apply**")
target_model_raw = st.selectbox(
    label="Specify the target models",
    options=model_names,
    key="target_model",
)
target_model = model_names_dict[target_model_raw]

# make prediction on the new data set
st.markdown("**Applied selected model to make prediction**")
y_future = saved_results[target_model].selected_model_.predict(X_future)
y_future = pd.Series(y_future, index=new_data.index)
st.write("A sample of the predicted y is shown as follows: ")
st.write(y_future.sample(10))

# save the results
save_button = st.button("Save the prediction")
if save_button:
    name = "new_data_prediction_" + target_model_raw + ".csv"
    path2future = os.path.join(target_path, name)
    y_future.to_csv(path2future)
    st.markdown(f"**The path to the saved prediction is: `{path2future}`.**")

# %% Evaluate the model's prediction performance if label is avaliable
st.header("Evaluate the model's prediction performance if label is avaliable")
st.write(
    "If the label of the new data is avaliable, we can also evaluate the performance of the prediction."
)

label_avaliable = st.button("Label in the new data is also avaliable")
if label_avaliable:
    st.session_state["label_avaliable"] = True

if "label_avaliable" in st.session_state:
    # infer the task
    prefix = list(saved_results.keys())[0].split("_")[0]
    task = "regression"
    if prefix == "clf":
        task = "classification"

    # specificy the outcome
    outcome = st.text_input("Specificy the outcome variable")
    if not outcome:
        st.warning("Please specify the outcome variable")
        st.stop()
    y_future_true = new_data.loc[:, outcome].to_numpy()

    # evaluate the model's performance
    selected_model = saved_results[target_model].selected_model_
    if task == "classification":
        eval_metrics = classification_evaluation_test_(
            selected_model, X_future, y_future_true
        )
    else:
        eval_metrics = regression_evaluation_test_(
            selected_model, X_future, y_future_true
        )

    st.markdown("**The selected model's performance on this new data set is: **")
    st.write(eval_metrics)
