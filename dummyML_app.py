# %% load required packages
# web application related functions
import streamlit as st
import tkinter as tk
from streamlit_pandas_profiling import st_profile_report

# import dummyML related functions
import os
import shutil
import joblib
import zipfile
import pandas as pd
from dummyML.preprocessing import data_preprocessing
from dummyML.utilities import intersection, setdiff, split_data, infer_task
from dummyML.automate_modeling_evaluation import automate_modeling
from dummyML.automate_modeling_evaluation import automate_evaluation

# import utility functions to support the wbe application
import codes.utilities as utilities
from codes.extract_features_from_models import extract_features
from codes import modeling_evaluating
from codes.generate_ROCs import binary_ROCs_evaluation

import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import seaborn as sns

# %% title page
st.title("dummyML: Automated data analysis pipelines on tabular data for non-experts")

# %% introduction page
st.header("Introduction")

# read into the introduction file at the begining of the web application
current_path = os.path.dirname(os.path.abspath(__file__))
st.markdown(
    utilities.get_file_content_as_string(
        os.path.join(current_path, "docs/introduction.md")
        )
    )

# %% Upload your data set
st.header("Upload your raw data")
st.markdown("In this section, you need to upload your raw data set in csv format for zipped csv format.")

# upload file button
st.markdown("**Please upload your data and specificy the outcome variable**")
st.warning(
    "The data should only contains the IDs (in the first column), the features and the outcome."
)

uploaded_file = st.file_uploader("Upload your data set", type=["csv", "zip"])
if not uploaded_file:
    st.warning("Please upload the data in csv or zipped csv format.")
else:
    # infer the compression style
    data_suffix = uploaded_file.name.split(".")[-1]
    compression = utilities.infer_compression(uploaded_file.name)
    experiment_name = uploaded_file.name.split(".")[0]
    data = utilities.load_data(uploaded_file, compression)

# Specificy the outcome variable
outcome = st.text_input("Specificy the outcome variable")
if not outcome:
    st.warning("Please specify the outcome variable")

# stop if data is not uploaded or outcome is not specified
if (not uploaded_file) or (not outcome):
    st.warning("Please upload your data and specify the outcome.")
    st.stop()
st.success("You data is ready to be casted into automated data analysis piplines!")

# basic information of the raw data
st.markdown("**Basic information of your data**")
st.write(f"The size of the raw data is `{data.shape}`")
st.write(f"The outcome variable is `{outcome}`")

# %% summarize the data set
st.header("Summarize the raw data")
st.markdown("""In this section, you can get a quick summary of your raw data set. 
  If the number of samples are larger than 1000, to save resouce, only a random subset of 1000 samples are
  used to summary the data.""")

# whether to show an example of the raw data set
st.markdown("**A sample of the raw data is shown as follows**")

if st.checkbox("A sample of the raw data"):
    n_samples = st.number_input(
        "Select the number of samples you want to see",
        min_value=10,
        max_value=data.shape[0],
        value=min(100, data.shape[0]),
    )
    st.write(data.sample(n_samples))

# generate the report to summary the data
st.markdown("**The raw data is summarized as follows**")
checkbox_summary = st.checkbox("Data summary")
if checkbox_summary:
    profile_report = utilities.summarize_data(
        data, max_sample=min(1000, data.shape[0]), minimal=True
    )
    st_profile_report(profile_report)

# %% preprocess your data sets
st.header("Data preprocessing process")
st.markdown(
    """
    The key points of the data preprocessing process are as follows:

* All the samples with missing values at the outcome are removed.
* All the features with unique values less than a threshold, e.g., 15, are taken as categorical variables.
* The columns contain strings and over 15 unique values are taken as text data, and will be dropped.
* Samples and features with more than a threshold (e.g., 0.5) missing values are dropped.
* Missing values in categorical variables are taken as a new level.
* Two coding methods for categorical variable are used: dummmy coding and ordinal coding
* Median imputation is used to tackle the missing values in quantitative variables.
* Variables with a single unique value are dropped.
* The data preprocessing steps are saved and can be used for applying the same preprocessing steps on future test set.
"""
)

## The arguments in this section
st.markdown("**The arguments to control the data preprocessing behavior**")

# default parameters in this section
default_preprocessing = {
    "cat_levels_threshold": 6,
    "missing_threshold": 0.5,
    "for_future_test": 0,
    "verbose": 1,
    "dummy_coding": 1,
}

# use default values or not
preprocess_default_custommize = st.selectbox(
    "Using default values or customize the values by yourself?",
    options=["Default", "Customize"],
)
if preprocess_default_custommize == "Default":
    cat_levels_threshold = default_preprocessing["cat_levels_threshold"]
    missing_threshold = default_preprocessing["missing_threshold"]
    for_future_test = bool(default_preprocessing["for_future_test"])
    verbose = default_preprocessing["verbose"]
    dummy_coding = bool(default_preprocessing["dummy_coding"])

    # Meaning of the parameters
    if st.checkbox("Show the default values", key="data_preprocess_default_values"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/data_preprocess_parameters.md")
            )
        )
else:
    # Meaning of the parameters
    with st.expander("Meaning of the parameters"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/data_preprocess_parameters.md")
            )
        )

    # tips and tricks
    with st.expander("Tips & Tricks"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/data_preprocess_tips.md")
            )
        )

    # customize the parameters
    col1, col2, col3, col4, col5 = st.columns(5)
    with col1:
        cat_levels_threshold = st.number_input(
            "cat_levels_threshold",
            min_value=int(2),
            max_value=int(15),
            value=default_preprocessing["cat_levels_threshold"],
            step=int(1),
        )
    with col2:
        missing_threshold = st.number_input(
            "missing_threshold",
            min_value=0.0,
            max_value=0.9,
            value=default_preprocessing["missing_threshold"],
            step=0.1,
        )
    with col3:
        for_future_test = st.number_input(
            "for future test",
            min_value=int(0),
            max_value=int(1),
            value=default_preprocessing["for_future_test"],
        )
        for_future_test = bool(for_future_test)
    with col4:
        dummy_coding = st.number_input(
            "dummy_coding",
            min_value=int(0),
            max_value=int(1),
            value=default_preprocessing["dummy_coding"],
        )
    with col5:
        verbose = st.number_input(
            "verbose",
            min_value=int(0),
            max_value=int(1),
            value=int(1),
        )

# other hyperparameters in this step
save_results = False

# preprocess the data
X, y, feature_names, sample_index, saved_preprocess_steps = data_preprocessing(
    data,
    outcome,
    name=None,
    dummy_coding=dummy_coding,
    cat_levels_threshold=cat_levels_threshold,
    missing_threshold=missing_threshold,
    for_future_test=for_future_test,
    verbose=verbose,
    save_results=save_results,
)

## Codes of preprocessing
st.markdown("**The source code of data preprocessing process**")
with st.expander("Where to find the source code?"):
    st.markdown("https://gitlab.com/YipengUva/end2endml_pkg/-/tree/master/src/dummyML/data_preprocessing.py")

## Basic information of the preprocessed data
# The size of the preprocessed data
st.markdown("**Basic information of the preprocessed data**")
st.markdown(
    f"The size of the preprocessed data are: \
      design matrix **X** `{X.shape}`; outcome **y** `{y.shape}`"
)

# %% Prepare the training and test set
st.header("Split training and test set")
st.markdown(
    """
    The key points of the data splitting process are as follows:
    
    - **test_size** (float, optional): the proportion left for test set. **Defaults to 0.2.** The training data is (1-test_size). The model will be selected and fitted on the training data and test data will be used in the evaluation. When test_size is close to 0 (<=0.01), K-Fold CV will be used to evaluate the selected model.
    - **random_state** (int, optional): seed for randomness control. **Defaults to None.**
    """
)

## The arguments in this section
st.markdown("**The arguments to control the data preprocessing behavior**")

# default parameters in this section
default_train_test = {
    "test_size": 0.2,
    "random_state": "None",
}

train_test_split = st.selectbox(
    "Default values or customize the values for train and test split",
    options=["Default", "Customize"],
)
if train_test_split == "Default":
    test_size = default_train_test["test_size"]
    random_state = default_train_test["random_state"]
    if st.checkbox("Show the default values", key="train_test_split"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/data_split_parameters.md")
            )
        )
else:
    # Meaning of the parameters
    with st.expander("Meaning of the parameters"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/data_split_parameters.md")
            )
        )

    # tips and tricks
    with st.expander("Tips & Tricks"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/data_split_tips.md")
            )
        )

    # options
    col1, col2 = st.columns(2)
    with col1:
        test_size = st.number_input(
            "test_size",
            min_value=0.0,
            max_value=0.5,
            value=default_train_test["test_size"],
            step=0.01,
        )
    with col2:
        random_state = st.text_input(
            "random_state", value="None", help="Input an integer, default=None"
        )
    st.caption("💭 Help")
    col1help_split, col2help_split = st.columns(2)
    with col1help_split:
        if st.button("Meaning of the Parameters"):
            st.markdown(
                "**test_size:** The proportion of the test set to the total data set."
            )
            st.markdown("**random_state:** A random seed.")
    with col2help_split:
        if st.button("Tips and Tricks"):
            st.markdown(
                "**test_size**: We usually set it to 0.20 so the ratio of train set and test set will be 4:1."
            )
            st.markdown(
                "**random_state**:If the same seed is set, you can get the same split results every time you run. "
                "If it is set to None, you can get different splits. When you want to select the models or parameters, "
                "we recommend you to set it to your favorite number."
            )
if random_state == "None":
    random_state = None
elif eval(random_state) == int or eval(random_state) == float:
    random_state = int(random_state)
else:
    st.error("random_state must be a number or None.")
    random_state = None

## show codes of split_data
st.markdown("**The source code of data splitting**")
with st.expander("Where to find the source code?"):
    st.markdown("https://gitlab.com/YipengUva/end2endml_pkg/-/tree/master/src/dummyML/utilities.py")

# split train and test set
X_train, X_test, y_train, y_test = split_data(
    X, y, test_size=test_size, random_state=random_state
)

# %% selected and fit the model
st.header("Select and fit the user specified models")
st.markdown("**The following models are included in this web application**")
st.markdown(
    utilities.get_file_content_as_string(
        os.path.join(current_path, "docs/models_names.md")
    )
)

st.markdown("**Tips and tricks in selecting the models**")
st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/model_selection_tips.md")
            )
        )

models = st.multiselect(
    "**Target models**",
    options=utilities.model_names,
    # default=["Linear", "Gradient boosting"],
    key="specified models",
)
st.write(f"Specified models are `{models}`")

# post process the model names for inputs to the Python package
inverse_model_names = dict((value, key) for key, value in utilities.model_names.items())
models_py = [utilities.model_names[ele] for ele in models]

## select and fit the models
st.markdown("**The arguments to control the model selection and fitting behavior**")

# set up some of the importance parameters
default_models_parameters = {
    "scaler": "standard",
    "decomposer": "None",
    "n_components": "None",
    "imbalance": 1,
    "imbalance_force": 0,
    "cv": 10,
    "cv_force": 0,
    "n_trials": 30,
    "n_jobs": 10,
    "max_iter": 100,
}

default_models = st.selectbox(
    "Default values or customize for models",
    options=["Default", "Customize"],
    key="default_models",
)
if default_models == "Default":
    scaler = default_models_parameters["scaler"]
    decomposer = default_models_parameters["decomposer"]
    n_components = default_models_parameters["n_components"]
    imbalance = default_models_parameters["imbalance"]
    imbalance_force = default_models_parameters["imbalance_force"]
    cv = default_models_parameters["cv"]
    cv_force = default_models_parameters["cv_force"]
    n_trials = default_models_parameters["n_trials"]
    n_jobs = default_models_parameters["n_jobs"]
    max_iter = default_models_parameters["max_iter"]

    if st.checkbox("Show the default values", key="train_method"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/model_selection_parameters.md")
            )
        )

else:
    # Meaning of the parameters
    with st.expander("Meaning of the parameters"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/model_selection_parameters.md")
            )
        )

    # options
    col11, col12, col13 = st.columns(3)
    with col11:
        scaler = st.selectbox(
            "scaler",
            options=["standard", "min_max", "max_abs", "robust"],
            key="scaler",
            help="Check 'Meaning of the parameters'",
        )
    with col12:
        cv = st.number_input(
            "cv",
            min_value=int(5),
            value=int(10),
            help="Check 'Meaning of the parameters'",
        )
    with col13:
        cv_force = st.number_input(
            "cv_force",
            min_value=int(0),
            max_value=int(1),
            value=int(0),
            help="Check 'Meaning of the parameters'",
        )
    col21, col22, col23 = st.columns(3)
    with col21:
        decomposer = st.selectbox(
            "decomposer",
            options=["None", "pca", "fast_ica", "nmf", "kernel_pca"],
            key="decomposer",
            help="Check 'Meaning of the parameters'",
        )
    with col22:
        n_components = st.text_input(
            "n_components",
            value="None",
            help="Check 'Meaning of the parameters'",
        )
    with col23:
        imbalance = st.number_input(
            "imbalance",
            min_value=int(0),
            max_value=int(1),
            value=int(1),
            help="Check 'Meaning of the parameters'",
        )
    col31, col32, col33, col34 = st.columns(4)
    with col31:
        n_trials = st.number_input(
            "n_trials",
            value=int(30),
            help="Check 'Meaning of the parameters'",
        )
    with col32:
        n_jobs = st.number_input(
            "n_jobs",
            value=int(10),
            help="Check 'Meaning of the parameters'",
        )
    with col33:
        imbalance_force = st.number_input(
            "imbalance_force",
            min_value=int(0),
            max_value=int(1),
            value=int(0),
            help="Check 'Meaning of the parameters'",
        )
    with col34:
        max_iter = st.number_input(
            "max_iter",
            min_value=int(100),
            value=int(100),
            help="Check 'Meaning of the parameters'",
        )

if decomposer == "None":
    decomposer = None
if n_components == "None":
    n_components = None
elif eval(n_components) == int or eval(n_components) == float:
    n_components = int(n_components)
else:
    st.error("n_components must be a number or None.")
    n_components = None
imbalance = bool(imbalance)
imbalance_force = bool(imbalance_force)
cv_force = bool(cv_force)

# select and fit the model
st.markdown("**Select and fit the models on training data**")
run_botton = st.button("Run")

# Codes of training
st.markdown("**Where to find the source code of select and fit models?**")
with st.expander("Where to find the source code?"):
    st.markdown("https://gitlab.com/YipengUva/end2endml_pkg/-/tree/master/src/dummyML/select_fit_models")

if run_botton:
    # select and fit the models on training data
    results = automate_modeling(
            X_train,
            y_train,
            scaler=scaler,
            decomposer=decomposer,
            n_components=n_components,
            models=models_py,
            imbalance=imbalance,
            imbalance_force=imbalance_force,
            cv=cv,
            cv_force=cv_force,
            n_trials=n_trials,
            n_jobs=n_jobs,
            max_iter=max_iter,
            verbose=verbose,
    )
    st.session_state.results = results
    # update the evaluation plots
    if "models_ROCs" in st.session_state:
        st.session_state.pop("models_ROCs")
    if "models_metrics" in st.session_state:
        st.session_state.pop("models_metrics")

if "results" not in st.session_state:
    st.warning("Please do the model selection and fitting before go to next steps.")
    st.stop()
results = st.session_state["results"]
fitted_models = [ele.split("_")[-1] for ele in results.keys()]
fitted_models = [inverse_model_names[ele] for ele in fitted_models]
st.success(f"The following models {fitted_models} have been selected and fitted.")

# %% evaluate the selected models
st.header("Evaluate the selected models")
st.markdown(
    """
    The selected models can be evaluated on the test set or thought 10-Fold cross validation.
    """
)

st.markdown("**Tips and tricks in evaluating the models**")
st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/model_evaluation_tips.md")
            )
        )


# options
st.markdown("**The arguments to control the model evaluation behavior**")

# default parameters in this section (evaluation section)
default_eval = {"eval_test": 1, "eval_cv": 0, "cv": 10, "n_jobs": 10}

eval_method = st.selectbox(
    "Default parameters or customize the parameters for train and test splitting",
    options=["Default", "Customize"],
    key="eval_key",
)
if eval_method == "Default":
    eval_test = default_eval["eval_test"]
    eval_cv = default_eval["eval_cv"]
    eval_cv_k = default_eval["cv"]
    eval_n_jobs = default_eval["n_jobs"]
    if st.checkbox("Show the default values", key="eval_method"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/model_evaluation_parameters.md")
            )
        )
else:
    # Meaning of the parameters
    with st.expander("Meaning of the parameters"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/model_evaluation_parameters.md")
            )
        )

    # customize the parameters
    col1, col2 = st.columns(2)
    with col1:
        eval_test = st.number_input(
            "eval_test",
            min_value=int(0),
            max_value=int(1),
            value=default_eval["eval_test"],
        )
    with col2:
        eval_cv = st.number_input(
            "eval_cv", min_value=int(0), max_value=int(1), value=default_eval["eval_cv"]
        )

    col3, col4 = st.columns(2)
    with col3:
        eval_cv_k = st.number_input(
            "cv", min_value=int(5), max_value=int(10), value=default_eval["cv"]
        )
    with col4:
        eval_n_jobs = st.number_input(
            "n_jobs", min_value=int(0), max_value=int(10), value=default_eval["n_jobs"]
        )

eval_test = bool(eval_test)
eval_cv = bool(eval_cv)

# test evaluation or cv evaluation or all of them
eval_test = True if test_size > 0.01 else False
if (not eval_cv) and (test_size < 0.01):
    eval_cv = True

# evaluate the models' performance on test set of CV
st.markdown("**The selected models' performance on the test set and/or on K-Fold CV**")

# When it is a classification problem, show AUC ROC figure 
if infer_task(y) == "binary_clf":
    eval_figure, eval_table = st.columns(2)
else:
    eval_table = st.columns(1)

if infer_task(y) == "binary_clf":
    with eval_figure:
        eval_figure_button = st.button("Model evaluation: AUC-ROC")
        if eval_figure_button:
            try:
                models_ROCs = binary_ROCs_evaluation(
                    results,
                    X_test,
                    y_test,
                    X,
                    y,
                    eval_test=eval_test,
                    eval_cv=eval_cv,
                    cv=eval_cv_k,
                    n_jobs=eval_n_jobs,
                )
                st.session_state["models_ROCs"] = models_ROCs
                st.write("AUC-ROC evaluation is done.")
            except Exception as e:
                if "BrokenProcessPool" in str(
                    e.args
                ) or "A worker process managed by the executor was unexpectedly terminated." in str(
                    e.args
                ):
                    st.error(
                        "Something wrong with parallel processes. Please customize n_jobs to 1."
                    )
                else:
                    st.error(e.args)

with eval_table:
    eval_table_button = st.button("Model evaluation: complete")
    if eval_table_button:
        try:
            models_metrics = automate_evaluation(
                results,
                X_test,
                y_test,
                X,
                y,
                eval_test=eval_test,
                eval_cv=eval_cv,
                cv=eval_cv_k,
                n_jobs=n_jobs,
            )
            st.session_state["models_metrics"] = models_metrics
            st.write("Complete evaluation is done.")
        except Exception as e:
            if "BrokenProcessPool" in str(
                e.args
            ) or "A worker process managed by the executor was unexpectedly terminated." in str(
                e.args
            ):
                st.error(
                    "Something wrong with parallel processes. Please customize eval_n_jobs to 1."
                )
            else:
                st.error(e.args)

if "models_metrics" not in st.session_state and "models_ROCs" not in st.session_state:
    st.warning("Please click the evaluation buttons to evaluate the selected models!")
    st.stop()
st.success("The selected models have been evaluated.")

# Using figure or table to show the results
st.markdown("**Show the selected models' performance**")

# get the true model names
show_metrics = st.selectbox(
    "Show the results using a figure or a table?", options=["Table", "Figure"]
)
if show_metrics == "Figure":
    if infer_task(y) != "binary_clf":
        st.warning("Figure result is not avaliable")
    elif "models_ROCs" not in st.session_state:
        st.warning(
            "Please first click button Model Evaluation: AUC-ROC to see the figures."
        )
        st.stop()
    if eval_test:
        st.write("Test set ROCs: ")
        models_fpr_tpr = st.session_state["models_ROCs"]["test_ROCs"]
        display = utilities.plot_ROCs(models_fpr_tpr, fitted_models)
        st.pyplot(display.figure_)

    if eval_cv:
        st.write("K-Fold CV ROCs: ")
        models_fpr_tpr = st.session_state["models_ROCs"]["cv_ROCs"]
        display = utilities.plot_ROCs(models_fpr_tpr, fitted_models)
        st.pyplot(display.figure_)

else:
    with st.expander("Help for reading the table"):
        st.markdown(
            utilities.get_file_content_as_string(
                os.path.join(current_path, "docs/help_metrics_names.md")
            )
        )
    if "models_metrics" not in st.session_state:
        st.warning(
            "Please first click button Model Evaluation: complete to see the tables of results."
        )
        st.stop()
    st.write(st.session_state["models_metrics"])

# Codes of evaluation
st.markdown("**Where to find the source code of model evaluation process?**")
with st.expander("Click to see the source code of model evaluation process"):
    st.markdown("https://gitlab.com/YipengUva/end2endml_pkg/-/tree/master/src/dummyML/evaluate_models")

# %% save the selected models and evaluation metrics
st.header("Save the data preprocessing steps and selected models")
st.markdown(
    """
    The the data preprocessing procedures, the selected models, and the associated 
    performance measures can be saved as a pickle file, then you can load the results later.
    """
)

# save the selected models and evaluation metrics
import pickle

#st.write(pickle.dumps(results))

saved_preprocess_steps_and_selected_models = {
    "saved_preprocess_steps": saved_preprocess_steps,
    "saved_selected_models": results,
}

st.download_button(
    label="Download the saved data preprocess steps and selected models",
    data=pickle.dumps(saved_preprocess_steps_and_selected_models),
    file_name="saved_preprocess_steps_and_selected_models.pkl",
    mime="application/octet-stream",
)

# %% Explore the results
st.header("Explore the selected models")

# models used in this experiment
st.markdown("**Select the models you want to explore**")
target_models_raw = st.multiselect(
    "Specify the target models",
    options=models,
    # default="Linear",
    key="target_models",
)
target_models_raw = [utilities.model_names[ele] for ele in target_models_raw]
prefix = list(results.keys())[0].split("_")[0]
target_models = [prefix + "_" + ele for ele in target_models_raw]
target_models = intersection(target_models, list(results.keys()))
# the model name for ordering the feature importance
order_key_raw = st.selectbox(
    "The model name for ordering the feature importance",
    options=models,
    index=0,
)
order_key = utilities.model_names[order_key_raw]

if order_key in target_models_raw:
    st.markdown(
        f"The feature importance or coefficient of `{order_key_raw}` model will be used to order the feature importance matrix."
    )
else:
    st.markdown(
        f"**The selected model name for ordering is not avaliable. Please select a model in the model list.**"
    )
    st.stop()

# get all the feature importance or coefficients for the target models
models_feature_importances = extract_features(
    results, target_models, feature_names, order_key=order_key
)

st.markdown("**The feature importance or coefficient for the target models**")
st.write(models_feature_importances)
st.bar_chart(models_feature_importances)
