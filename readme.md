# Web application for dummyML project

### Introduction

We developed an dummyML Python package for end-to-end machine learning on administrative data analysis. The homepage of the developed Python package is https://gitlab.com/YipengUva/dummyML_pkg. However, currently, only people with Python programming skills and basic machine learning background can make full use of the developed package for doing data analysis. For dummies or data analysts without a lot of Python programming skills, there is a need for an easily accessible pipeline to do quick data analysis. 

The objective of this project is to develop a web application based on dummyML package for doing automatic data analysis on tabuolar. The web application should be easily accessible for dummies without machine learning and programming skills, and data analysts without a lot of Python programming skills.

### Set up development environment 

###### Virtual environment

We will use the virtual environment to isolate the development environment. The configuration of the virtual environment is the web_app.yaml. You can recreate the virtual environment in your local machine by `conda env create --file web_app.yaml`. 

In the new version of dummyML package, version 0.9.3, the function to let the user to specify the working directory is added. Thus, you have to update dummyML package. Using `pip install dummyML==0.9.4.dev0` to do it.

### Installation

Detailed instructions for installation the web application in your local PC or local server will be in this section.

### To do list

