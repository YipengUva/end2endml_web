import streamlit as st
from dummyML.automate_modeling_evaluation import automate_modeling
from dummyML.automate_modeling_evaluation import automate_evaluation

# define a function to cache the automate_modeling process
@st.cache(allow_output_mutation=True)
def cached_automate_modeling(
    X_train,
    y_train,
    scaler,
    decomposer,
    n_components,
    models,
    imbalance,
    imbalance_force,
    cv,
    cv_force,
    n_trials,
    n_jobs,
    max_iter,
    verbose,
):
    results = automate_modeling(
        X_train,
        y_train,
        scaler=scaler,
        decomposer=decomposer,
        n_components=n_components,
        models=models,
        imbalance=imbalance,
        imbalance_force=imbalance_force,
        cv=cv,
        cv_force=cv_force,
        n_trials=n_trials,
        n_jobs=n_jobs,
        max_iter=max_iter,
        verbose=verbose,
    )
    return results


# cache the automate evaluation process
# @st.cache(allow_output_mutation=True)
def cached_automate_evaluation(
    results, X_test, y_test, X, y, eval_test, eval_cv, cv, n_jobs
):
    models_metrics = automate_evaluation(
        results,
        X_test,
        y_test,
        X,
        y,
        eval_test=eval_test,
        eval_cv=eval_cv,
        cv=cv,
        n_jobs=n_jobs,
    )
    return models_metrics
