# -*- coding: utf-8 -*-
"""
Created on Sun Sep 14 13:41:26 2021

This file contains the functions to generate ROCs for classification models

@author: yipeng.song@hotmail.com, Junyun
"""
# %% load into required packages
from sklearn import metrics
import numpy as np
from sklearn import model_selection

# %% define a function to evaluate ROC-AUCs
def binary_ROCs_evaluation(
    results, X_test, y_test, X, y, eval_test, eval_cv, cv=10, n_jobs=10
):
    models_fpr_tpr_test = None
    if eval_test:
        models_fpr_tpr_test = classification_evaluation_test(results, X_test, y_test)

    models_fpr_tpr_cv = None
    if eval_cv:
        models_fpr_tpr_cv = classification_evaluation_cv(
            results, X, y, cv=cv, n_jobs=n_jobs
        )

    models_ROCs = {"test_ROCs": models_fpr_tpr_test, "cv_ROCs": models_fpr_tpr_cv}
    return models_ROCs


# %% binary classification: test set evaluation
def classification_evaluation_test(results, X_test, y_test):
    """evaluate the selected models' performance on the test set

    Args:
        results (dict): key, model name; value, saved models and model selection.
        X_test (np.ndarray): test set X
        y_test (np.ndarray): test set y

    Returns:
        pd.dataframe: multiple performance metrics for all the saved models.
    """
    # extract the keys in the dict of the results
    model_names = list(results.keys())

    # get the metrics on the test set for all the selected models
    models_fpr_tpr_test = {}
    for key in model_names:
        selected_model = results[key].selected_model_
        models_fpr_tpr_test[key] = classification_evaluation_test_(
            selected_model, X_test, y_test
        )

    return models_fpr_tpr_test


# %% binary classification: K-Fold CV evaluation
def classification_evaluation_cv(results, X, y, cv=10, n_jobs=10):
    """evaluate the selected models' performance using K-Fold CV

    Args:
        results (dict): key, model name; value, saved models and model selection.
        X (np.ndarray): X
        y (np.ndarray): y
        cv (int, optional): K-Fold CV. Defaults to 10.
        n_jobs (int, optional): number of cores to be used. Defaults to 10.

    Returns:
        pd.dataframe: multiple performance metrics for all the saved models.
    """
    # extract the keys in the dict of the results
    model_names = list(results.keys())

    # get the metrics on all the data using K-Fold CV for the models
    models_fpr_tpr_cv = {}
    for key in model_names:
        selected_model = results[key].selected_model_
        models_fpr_tpr_cv[key] = classification_evaluation_cv_(
            selected_model, X, y, cv=cv, n_jobs=n_jobs
        )

    return models_fpr_tpr_cv


# %% classification evaluation for a single model: test set
def classification_evaluation_test_(selected_model, X_test, y_test):
    # get task, y_true, y_hat, y_hat_prob
    y_true, y_hat_prob = predict_test_set_(selected_model, X_test, y_test)

    # metrics on the test set
    model_fpr_tpr = binary_evaluation_base_(y_true, y_hat_prob)

    return model_fpr_tpr


# %% classification evaluation for a single model: K-Fold CV
def classification_evaluation_cv_(selected_model, X, y, cv=10, n_jobs=10):
    # get task, y_true, y_hat, y_hat_prob
    y_true, y_hat_prob = predict_cv_set_(selected_model, X, y, cv, n_jobs)

    # metrics on the test set
    model_fpr_tpr = binary_evaluation_base_(y_true, y_hat_prob)
    return model_fpr_tpr


# %% base evaluators
def binary_evaluation_base_(y_true, y_hat_prob):
    pos_label = np.unique(y_true)[1]
    AUC = np.nan
    fpr = np.array([])
    tpr = np.array([])
    if y_hat_prob is not None:
        AUC = metrics.roc_auc_score(y_true, y_hat_prob)
        fpr, tpr, _ = metrics.roc_curve(y_true, y_hat_prob, pos_label=pos_label)

    # metrics
    model_fpr_tpr = {"fpr": fpr, "tpr": tpr, "AUC": AUC}

    return model_fpr_tpr


# %% define functions to generate y_hat_prob for test set or in K-Fold CV
# def a fun to predict the X y in the CV way
def predict_cv_set_(selected_model, X, y, cv, n_jobs):
    # check it is binary or multiclass
    task = "binary"
    if len(np.unique(y)) > 2:
        task = "multiclass"

    # y_true, y_hat_prob
    y_true = y

    y_hat_prob = None
    # probability estimation
    if hasattr(selected_model, "predict_proba") and task == "binary":
        y_hat_prob = model_selection.cross_val_predict(
            selected_model, X, y, cv=cv, n_jobs=n_jobs, method="predict_proba"
        )[:, 1]

    # non probability estimation for svm model
    if hasattr(selected_model, "decision_function") and task == "binary":
        y_hat_prob = model_selection.cross_val_predict(
            selected_model, X, y, cv=cv, n_jobs=n_jobs, method="decision_function"
        )
    return y_true, y_hat_prob


# def a fun to predict the test set
def predict_test_set_(selected_model, X_test, y_test):
    # check if it is binary or multiclass
    task = "binary"
    if len(np.unique(y_test)) > 2:
        task = "multiclass"

    # y_true, y_hat, y_hat_prob
    y_true = y_test
    y_hat_prob = None

    # get the probability estimation
    if hasattr(selected_model, "predict_proba") and task == "binary":
        y_hat_prob = selected_model.predict_proba(X_test)[:, 1]

    # for svm, it is not probability but it can still be used for generate AUC
    if hasattr(selected_model, "decision_function") and task == "binary":
        y_hat_prob = selected_model.decision_function(X_test)

    return y_true, y_hat_prob
