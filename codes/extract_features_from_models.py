# -*- coding: utf-8 -*-
"""
This file contains the functions to extract features from saved models
"""
import numpy as np
import pandas as pd
from dummyML.utilities import intersection

# def a function to extract the features from a single model and rank the features
def extract_rank_features(saved_model, feature_names):
    # extract the feature importances of the features
    feature_importance = saved_model.feature_importances_

    if len(feature_importance.shape) > 1:
        raise ValueError("The fun can only be used for vector form feature importance")

    feature_importance = pd.Series(feature_importance, index=feature_names)
    order = feature_importance.abs().sort_values(ascending=False)
    feature_importance = feature_importance[order.index]
    return feature_importance


# def a function to extract the features from multiple models
def extract_features(saved_models, model_names, feature_names, order_key="linear"):
    # extract the keys in the dict of the results
    full_names = list(saved_models.keys())
    model_names = intersection(model_names, full_names)

    # get the metrics on the test set for all the selected models
    models_feature_importances = {}
    for key in model_names:
        feature_importance = saved_models[key].feature_importances_
        if len(feature_importance.shape) > 1:
            raise ValueError(
                "The fun can only be used for vector form feature importance"
            )

        models_feature_importances[key] = feature_importance

    # generate the model evaluation metrics
    models_feature_importances = pd.DataFrame(models_feature_importances)
    models_feature_importances.index = feature_names

    # order the features according to the specified key
    order_key = model_names[0].split("_")[0] + "_" + order_key
    order_feature_importance = models_feature_importances.loc[:, order_key]
    ordered_feature_importance = order_feature_importance.abs().sort_values(
        ascending=False
    )

    return models_feature_importances.loc[ordered_feature_importance.index, :]
