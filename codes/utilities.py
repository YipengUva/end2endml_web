# -*- coding: utf-8 -*-
"""
This file contains the functions to support the web application
"""
# %% required packages
import enum
import sys
import streamlit as st
from contextlib import contextmanager
from io import StringIO
#from streamlit.report_thread import REPORT_CONTEXT_ATTR_NAME
from threading import current_thread
import pandas as pd
import pandas_profiling
from sklearn import metrics
import matplotlib.pyplot as plt

# %% define a function to read the markdown file as a string
def get_file_content_as_string(path2file):
    """code used to test the fun
    path2file = "./readme.md"
    print(get_file_content_as_string(path2file))
    """
    plain_text = open(path2file, "r", encoding="utf-8").read()
    return plain_text


# %% function to read the loaded data as dataframe
@st.cache(allow_output_mutation=True)
def load_data(uploaded_file, compression=None):
    data = pd.read_csv(uploaded_file, index_col=0, compression=compression)
    return data


# cache the report generating process
@st.cache(allow_output_mutation=True)
def summarize_data(data, max_sample=1000, minimal=True):
    profile_report = pandas_profiling.ProfileReport(
        data.sample(min(max_sample, data.shape[0])), minimal=minimal, explorative=True
    )
    return profile_report


# %% infer the compression type
def infer_compression(name):
    data_suffix = name.split(".")[-1]
    compression = None
    if data_suffix == "zip":
        compression = "zip"
    return compression


# %% define a map for model names
model_names = {
    "Linear": "linear",
    "Lasso": "lasso",
    "Ridge": "ridge",
    "ElasticNet": "elasticNet",
    "Support vector machines": "svm",
    "Neural network": "nn",
    "Gradient boosting": "gb",
    "Random forest": "rf",
}

# %% def a fun to generate the figure
def plot_ROCs(models_fpr_tpr, model_names):
    fig, ax = plt.subplots(figsize=(5, 5))
    ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))
    for i, model in enumerate(list(models_fpr_tpr.keys())):
        display = metrics.RocCurveDisplay(
            fpr=models_fpr_tpr[model]["fpr"],
            tpr=models_fpr_tpr[model]["tpr"],
            roc_auc=models_fpr_tpr[model]["AUC"],
            estimator_name=model_names[i],
        )
        display.plot(ax)
    return display
