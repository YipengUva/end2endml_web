
FROM python:3.9-slim

EXPOSE 8501

WORKDIR /app

RUN apt-get update && apt-get install -y \
    build-essential \
    software-properties-common \
    git \
    python-tk \
    && rm -rf /var/lib/apt/lists/*

RUN echo "rerun git clone again" && git clone https://gitlab.com/YipengUva/end2endml_web.git .

RUN pip install -r requirements.txt

ENTRYPOINT ["streamlit", "run", "dummyML_app.py", "--server.port=8501", "--server.address=0.0.0.0", "--server.maxUploadSize=10240"]