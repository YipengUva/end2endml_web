All the bool arguments are replaced by integers in the web application. False: 0; True: 1.

- **eval_test** (bool, optional): whether to evaluate the selected models on test set. **Defaults to True.**
- **eval_cv** (bool, optional): whether to evaluate the selected models using K-Fold CV. **Defaults to False.**
- **cv** (int, optional): K-Fold CV. **Defaults to 10.**

