The source code for the model selection and fitting process is avaliable at https://gitlab.com/YipengUva/end2endml_pkg/-/tree/master/src/end2endML/select_fit_models

**Key points**

* Depends on the data type of outcome y and its unique values, the
  modelling will be classified as the following tasks, regression,
  binary and multiclass classification.
* The following models, standard linear model (linear), linear model
  with lasso penalty (lasso), linear model with ridge penalty (ridge);
  linear model with ElasticNet penalty (elasticNet), support vector
  machine (svm), neural network (nn), gradient boosting (gb), random
  forest (rf) are installed for all the above three tasks.
* When imbalances is high, majority category is over 10 times of the
  minority category, ensemble based imbalanced learning models, balanced 
  random forest model, Random under-sampling integrated in the learning 
  of AdaBoost and Bag of balanced boosted learners, are used to model the data.
* For lasso, ridge and elasticNet, the model selection is based on the 
  model selection procedures provided by sklearn.
* For svm, nn, gb and rf, Bayesian optimization is used to do the model 
  selection. The evaluation is based on either K-Fold CV or the performance 
  on the validation set.