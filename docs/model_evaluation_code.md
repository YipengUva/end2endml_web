The source code for evaluating the selected models is avaliable at https://gitlab.com/YipengUva/end2endml_pkg/-/tree/master/src/end2endML/evaluate_models

**key points**

* The selected model can be evaluated on the test set and (or) through K-Fold CV.
* For the classification problem, these metrics, sensitivity, specificity, balanced_accuracy, recall, precision, f1_score and AUC, are used.
* For regression problem, these metrics, :math:`R^2`, mean squared error (MSE) and mean absolute error (MAE), are used.