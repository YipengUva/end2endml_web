All the bool arguments are replaced by integers in the web application. False: 0; True: 1.

- **cat_levels_threshold** (int, optional): When the unique values of a column is less than this value, will take it as a categorical variable. **Defaults to 6**.
- **missing_threshold** (float, optional): [0, 1], When the propotation of missing values larger than this value, remove the corresponding rows and columns. **Defaults to 0.5**.
- **for_future_test** (bool, optional): If the saved preprocessing is going to be used to preprocess the future test data, it is better set it as True. This argument is mainly about how to do one-hot encoding. When it is True, the first level will not be dropped and unknown class in the test set will be ignored. **Default 1**.
- **dummy_coding** (bool, optional): Whether to use dummy coding for categorical variables? Defaults to True. If False, ordinal encoder, coding categorical variables as integers, will be used. For tree based model, ordinal encoder is also a popular option, while for linear models, ordinal encoder is not a good idea. **Default to 1**.
- **verbose** (int, optional): whether to show log information. 1: show information; 0: not show information. **Defaults to 1**.

