All the bool arguments are replaced by integers in the web application. False: 0; True: 1.

- When **for_future_test** is False, the saved preprocessing steps can still be applied on raw future data. However, when there is unknown class in the explnatory variables (or features), the saved preprocessing steps will generate error.
- When the focus is about prediction, setting **for_future_test** to True will not effect the prediction performance. However, when we need to explain the features, it is better to set **for_future_test** to False.
- When the variables with limited unique values are ordinal variables, there is no need to do one-hot encoding. In this situation, you can always set **cat_levels_threshold** to be 2, then only variables with two unique values and categorical variables will be coded as categorical variables.
- When tree based models are used, ordinal coding can be used. For linear methods, ordinal coding is not a good idea.
