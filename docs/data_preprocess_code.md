The source code for the data preprocessing process is avaliable at https://gitlab.com/YipengUva/end2endml_pkg/-/blob/master/src/end2endML/preprocessing.py

**Key points**

* All the samples with missing values at the outcome are removed.

* All the features with unique values less than a threshold, e.g., 15, are taken as categorical variables.

* The columns contain strings and over 15 unique values are taken as text data, and will be dropped.

* Samples and features with more than a threshold (e.g., 0.5) missing values are dropped.

* Categorical variables are one-hot coded with missing value as a new level and the first column is dropped.

* Median imputation is used to tackle the missing values in quantitative variables.

* Variables with a single unique value are dropped.

* The data preprocessing steps are saved and can be used for applying the same preprocessing steps on future test set.

  