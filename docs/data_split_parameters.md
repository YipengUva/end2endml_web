- **test_size** (float, optional): the proportion left for test set. **Defaults to 0.2.** The training data is (1-test_size). The model will be selected and fitted on the training data and test data will be used in the evaluation. When test_size is close to 0 (<=0.01), K-Fold CV will be used to evaluate the selected model.
- **random_state** (int, optional): seed for randomness control. **Defaults to None.**

