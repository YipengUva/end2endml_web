- When sample size is large, support vector machine and neural network are very computational intensive. 
- When sample size is small, e.g., less than 100, it is suggested to use the Leave-One-Out Cross Validation (LOOCV) as the metric in the model selection process. You can set the cv to the number of samples in training data.
- Shortcuts are used in the model selection of gradient boosting, random forest and neural netowrk to save time. When you really want to use the K-Fold CV to do model selection, you can set the **cv_force** to True.
- Other suggestions when sample size is very large.
  - The model selection of elasticNet is also computational intensive as the search space is quite large. Using ridge regression or lasso instead elasticNet.
  - Don't set **cv_force** to be True. 
