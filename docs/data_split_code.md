The source code to split the train and test sets is avaliable at https://gitlab.com/YipengUva/end2endml_pkg/-/blob/master/src/end2endML/utilities.py

**Key points**

* When test_size == 0, K-Fold CV will be used to evaluate the selected models.
* When test_size > 0, test size will be used to evaluate the selected models. In addition, K-Fold CV can also be used.
