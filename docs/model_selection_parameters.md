All the bool arguments are replaced by integers in the web application. False: 0; True: 1.

- **scaler** (str, optional): scaling method. **Defaults to 'standard'.** Other possibilities include "max_abs", "min_max", "robust". Check the sklearn scaler method to find the meaning of different choices.

- **cv** (int, optional): K-Fold Cross Validation (CV) for model selection. **Defaults to 10.**

- **cv_force** (bool, optional): whether to force the model selection to use K-Fold CV to evaluate the model during the model selection process. **Defaults to False.**

- **decomposer** (str, optional): dimension reduction method. **Defaults to None.** Other possibilities include "pca", "fast_ica", "nmf", "kernel_pca" with rbf kernel. Check the sklearn decomposition method to find the meaning of different choices.

- **n_components** (int, optional): the number of components in dimension reduction. **Defaults to None**, which is corresponding to int(0.5 * min(X.shape)).

- **imbalance** (bool, optional): whether to do imbalance classification. **Defaults to False.**

- **n_trials** (int, optional): number of Bayesian Optimization trials. **Defaults to 30.** 

- **n_jobs** (int, optional): number of cores to be used in model selection. **Defaults to 10.**

- **max_iter** (int, optional): maximum iterations for some models. **Defaults to 100.**

- **imbalance_force** (bool, optional): force to choose the imbalanced classification mdoels. imbalanced classification models include balanced random forest, RUSBoostClassifier, and EasyEnsembleClassifier. **Defaults to False.**

  

