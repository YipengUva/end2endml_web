* **Linear**: linear regression for continuous outcome; logistic linear regression for discrete outcome.

- **Lasso**: linear regression with L1 penalty for continuous outcome; logistic linear regression model with L1 penalty for discrete outcome.

- **Ridge**: linear regression with L2 penalty for continuous outcome; logistic linear regression model with L2 penalty for discrete outcome.

- **ElasticNet**: linear regression with ElasticNet (mixed L1 and L2) penalty for continuous outcome; logistic linear regression model with ElasticNet penalty for discrete outcome.

- **Support vector machines**: support vector machines model for both regression and classification.

- **Neural network**: neural network for both regression and classification.

- **Gradient boosting**: gradient boosting model for both regression and classification.

- **Random forest**: random forest model for both regression and clasification.

  

