We previously developed a Python package named "dummyML" for the automated data analysis of tabular data sets for non-experts. The homepage of the package is https://gitlab.com/YipengUva/end2endml_pkg. However, currently, only people with Python programming skills can make full use of the developed package. 

Based on the dummyML package, the dummyML web application is aimed to achieve automated data analysis on tabular data using the best practices in machine learning community without the requirement of python programming skills. The following two groups of people, non-experts without machine learning and Python programming background, and data analysts who don't know much about Python programming, are the target users when developing the web application.

The Cite information will be here. `TO DO`

If you found any bugs or encounter any problems, please report it at https://gitlab.com/YipengUva/end2endml_web/-/issues. 
